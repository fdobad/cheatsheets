__Several CheatSheets and installation & building guides__  
(debian; tmux, vim, python & jupyterlab oriented)

# Index:
- [apt & dpkg](apt.md)
- [Wireless cli setup w/o NetworkManager](cliWireless.md)
- [Building python OpenCV](compiling_OpenCV.md)
- [Building Coin-OR solver](compiling_coin-or_solvers.md)
- [distribution upgrade](distributionUpgrade.md)
- [font stuff](fonts.md)
- [git](git.md)
- [gnome pipewire loop](gnome.md)
- [grub](grub.md)
- [jupyter to pdf o html](pandoc.md)
- [redis server](redis.md)
- [sleep management](sleep.md)
- [ssh](ssh.md)
- [swap](swap.md)
- [systemd](systemd.md)


# vim tmux integration
 - Paste in Tmux: 'Ctrl-Shift-v' (paste the clipboard) or 'Shift-Insert' (paste the selection, Shift+Fn+Return in Mac).
 - Paste in Vim:  "+p (cut buffer) or "\*p (selection buffer).
https://romainpellerin.eu/copy-pasting-in-tmux-and-vim.html
Too much : https://github.com/brauner/vimtux

# vim
[more](vim.md)  

| command       | does     |  
| ------------- | ---------- |  
| (visual selection) ` "*y ` | copy to system clipboard |  
| \<C-o> and \<C-i> | Jump between last positions  |  
| * | selection buffer Primary and Secondary |
| + | cut buffer |

# tmux
```
# prefix changed from b->a
check ~/.tmux.conf

# attach or start one liner  
tmux new-session -A -s main
tmux attach || tmux new-session -n 'top' 'top'

# resize panes
C-a : resize-pane -D 5 "resize 10 down" UDLR <--tab autocompletes

# list current keys
C-a : list-keys
C-a ?
```
https://tmuxcheatsheet.com/  
https://romainpellerin.eu/copy-pasting-in-tmux-and-vim.html

# fzf
```
# include .hidden files
find . | fzf 
```
https://github.com/junegunn/fzf/blob/master/README-VIM.md

# sed
```
# (i)n filename (s)ubstitute (g)lobally (c)onfirming all instances of old with new
sed -i 's/old/new/gc' filename

# (e)xecute [chained commands] (d)elete [no print]
# '^#' starting with #
# '^ ' starting with space
# '^$' empty line
sed -e '/^ /d' -e '/^$/d' -e '/^#/d' filename
```

# python
## play sounds in python
```bash
$ apt install libasound2-dev
$ pip3 install simpleaudio
```  
https://github.com/hamiltron/py-simple-audio  

## venv
`NAME='myPyVenv'`
| command  | does  |
| - | - | 
| `python3 -m venv ~/env/$NAME` | make a new virtual environment |
| `python3 -m venv --help` | get help |
| `python3 -m venv --symlinks --upgrade --upgrade-deps ~/env/$NAME` | options to symlink & upgrade |
| `python3 -m venv --system-site-packages --symlinks --upgrade --upgrade-deps ~/env/$NAME` | share system site packages |
| `python3 -m pip install --upgrade pip wheel setuptools` | upgrade system pip |
| `vim ~/env/$NAME/pyenv.cfg` | see venv config |
|  |  |
| `source ~/env/$NAME/bin/activate` | activate venv |
| `echo "alias pyvenv='source ~/$NAME/dev/bin/activate'">>~/.bashrc` | activation shortcut alias |
| `echo "source ~/env/$NAME/bin/activate">>~/.bashrc` | auto activate on terminal startup |
| `pip3 list --outdated` | list pip outdated packages |

# jupyterlab
```
# extensions should match versions from these outputs
jupyter serverextension list
jupyter labextension list

# build problems
jupyter lab clean
jupyter lab build --debug
jupyter-lab build --dev-build=False --minimize=False

## memory build problems: did you kill earlyoom?
ps -fea | grep earlyoom
kill ...
```


# make a bootable debian live usb
- Download an (non-free-including-firmware) ISO, example: 
	https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/10.8.0-live+nonfree/amd64/bt-hybrid/debian-live-10.8.0-amd64-gnome+nonfree.iso.torrent
- Plug a destroyable usb stick (>4GB)
```
# identify dev/s
lsblk
# unmount
sudo umount /dev/sdX
# dd:disk destroy
sudo dd bs=4M if=/path/to/iso/debian-live....iso of=/dev/sdX status=progress oflag=sync
```

# hp print/scanner drivers
```
sudo apt install printer-driver-hpcups libsane-hpaio 
sudo systemctl start cups
```

