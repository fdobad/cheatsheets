__APT is for aptitude, DPKG is for debian package__

# some apt and dpkg commands
| command       | does     |  
| ------------- | ---------- |   
| `sudo apt update && upgrade` | check & upgrade |
| `sudo apt install --reinstall` | force reinstall |
| `apt list --installed` | list all installed packaged |
| `apt search <file_name>` | search for file|pkg |
| `dpkg-query -L <package_name>` | See all the files the package installed onto your system
| `dpkg-deb -c <package.deb>` | See the files a .deb file would install |
| `sudo dpkg -i <package.deb>` | Installs deb pkg |
| `dpkg -l \| grep <package.deb>` | Checks is installed? | 
| `sudo dpkg -r <package.deb>` | Removes w/o configuration |
| `sudo dpkg -P <package.deb>` | Purges all |

# Sources configuration  
```
sudo -e /etc/apt/sources.list /etc/apt/sources.list.d/*
```
## BULLSEYE
> deb http://deb.debian.org/debian bullseye main contrib non-free  
> deb-src http://deb.debian.org/debian bullseye main contrib non-free  
> 
> deb http://deb.debian.org/debian bullseye-updates main contrib non-free  
> deb-src http://deb.debian.org/debian bullseye-updates main contrib non-free  
> 
> deb http://security.debian.org/debian-security/ bullseye-security main contrib non-free  
> deb-src http://security.debian.org/debian-security/ bullseye-security main contrib non-free  

## BUSTER BACKPORTS
> deb http://deb.debian.org/debian buster-backports main contrib non-free  
> deb-src http://deb.debian.org/debian buster-backports main contrib non-free  

# Mark disable upgrades
``` 
# hold
sudo apt-mark hold *nvidia*
# show
apt-mark showhold
``` 

# Mess with repo priorities
[ref](https://gist.github.com/JPvRiel/8ae81e21ce6397a0502fedddca068507)
``` 
sudo -e /etc/apt/preferences.d/pinning 

Package: *
Pin: release o=Debian,a=stable
Pin-Priority: 10(sid) 999(stable)
```

# Add a ppa repo
[Signal](https://signal.org/es/download/#) example:
```
# 1. Install our official public software signing key
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null

# 2. Add our repository to your list of repositories
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
  sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
```

# Troubleshoot: Unable to acquire the dpkg frontend lock
``` 
# 0
sudo apt ...

# 1
ps aux | grep -i apt
    apt.systemd.daily ... wait
    sudo kill <process_id>
    
# 2 last resort
sudo lsof /var/lib/dpkg/lock
sudo lsof /var/lib/apt/lists/lock
sudo lsof /var/cache/apt/archives/lock

sudo kill -9 <process_id>

sudo rm /var/lib/apt/lists/lock
sudo rm /var/cache/apt/archives/lock
sudo rm /var/lib/dpkg/lock
sudo dpkg --configure -a
```
