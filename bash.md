# setup
Add `set -o vi` on your .bashrc

# some keybindings

| Key | Result                            |
| -- | -- |
| Esc | Enter vim normal mode |
| 0   | Move cursor to beginning of line. |
| $   | Move cursor to end of line.       |
| ^   | Move to the first non-blank character at the beginning of the line. |
| b   | Move cursor back one word.        |
| B   | Move back one word using space character as the delimiter. |
| w   | Move cursor right one word.       |
| h   | Move cursor back one character.   |
| l   | Move cursor right one character.  |
| k   | Move up in Bash command history.  |
| j   | Move down in Bash command history.|
| f<char> | Find occurrence of the next <char> |
| ;   | move to the next occurrence of the character. |
| .   | Repeat the last command |
| ~   | Toggle the case of the letter under the cursor |
| /   | search for commands |
