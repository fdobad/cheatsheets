_Command Line Network Managing_
# Check what's going on
```
lspci
ip a
```

# Setup dhcp
Edit the file `/etc/systemd/network/dhcp.network` to contain:
```
[Match]
Name=*

[Network]
DHCP=yes
```
# Setup a network interface
Edit the file `/etc/network/interfaces.d/<interface_name>` to contain:
```
allow hot-plug <interface_name>
iface <interface_name> inet loopback
```
# Add ssid credentials using wpa_supplicant
```
# 1 Generate tokenized password with 
$ wpa_passphrase <ssid>
# 2  add it to wpa_supplicant config
$ echo "$(wpa_passphrase <ssid> <passwd>)" | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf
```

# Restart & connect wifi
```
#!/bin/bash
sudo systemctl restart systemd-networkd networking
sudo wpa_supplicant -c /etc/wpa_supplicant/wpa_supplicant.conf -i <wlan0>
```

# Tired yet?
```
sudo apt install network-manager
# use from cli with
nmtui
```