# Manual build

```
git clone --recursive https://github.com/opencv/opencv-python.git

sudo apt install python3-wheel python3-pip python3-numpy ninja-build libgtk-3-dev gstreamer1.0-opencv libfreetype-dev libharfbuzz-dev libhdf5-dev liblapack-dev libmetis-dev libjpeg-dev libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libopengl-dev libpng-dev libopencl-clang-dev libffmpeg-ocaml-dev libgstreamer-ocaml-dev libgirepository1.0-dev libeigen3-dev libmpfrc++-dev libva-dev libgsl-dev libopenblas-dev libavresample-dev libtesseract-dev liblapack64-dev

export TMPDIR=~/source/opencv-python/bigAssSpace

export BUILD_opencv_apps=1
export ENABLE_CONTRIB=1
export BUILD_EXAMPLES=1
export INSTALL_PYTHON_EXAMPLES=ON
export OPENCV_ENABLE_NONFREE=1
export OPENCV_FORCE_3RDPARTY_BUILD=ON 
export WITH_FFMPEG=ON 
export WITH_GSTREAMER=ON 
export WITH_GTK=ON 
export WITH_OPENGL=ON 
export WITH_OPENMP=ON 
export WITH_PNG=ON 
export WITH_OPENJPEG=ON 
export WITH_V4L=ON 
export WITH_DSHOW=ON 
export WITH_OPENCL=ON 
export WITH_OPENCL_SVM=ON 
export WITH_OPENCL_D3D11_NV=ON 
export WITH_LAPACK=ON 
export WITH_QUIRC=ON 
export WITH_ONNX=ON
export WITH_EIGEN=ON

pip wheel . --verbose 2>&1 | tee log.txt

pip install numpy....whl opencv....whl

```
