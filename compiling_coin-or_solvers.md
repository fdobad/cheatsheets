Compile and install Ipopt and SHOT solvers
 - TO BE USED FROM PYOMO
 - Tested in debian buster 10.8 amd64
 - Linux fdeb 4.19.0-14-amd64 #1 SMP Debian 4.19.171-2 (2021-01-30) x86_64 GNU/Linux

# Ipopt
## build tools + solvers
```
sudo apt-get install gcc g++ gfortran git patch wget pkg-config cmake liblapack-dev libmetis-dev coinor-cbc 

# optional replace blas/lapack with non-free intel  
## (non-free must be enabled check /etc/apt/sources.list)
sudo apt install intel-mkl  
```

## Third party libs
### ASL
```
git clone https://github.com/coin-or-tools/ThirdParty-ASL.git
cd ThirdParty-ASL
./get.ASL
./configure
make
sudo make install
```

### HSL
Optional to get more solvers (and takes a few days to get the email back):
 - Get a download link from http://hsl.rl.ac.uk/ipopt  
 - Check the "Which solver?" section  
 - Unpack the HSL sources archive, move and rename the resulting directory so that it becomes ThirdParty-HSL/coinhsl  


```
git clone https://github.com/coin-or-tools/ThirdParty-HSL.git


# optional part begin
tar -xf coinhsl-2019.05.21.tar.gz 
mv coinhsl-2019.05.21 ThirdParty-HSL/coinhsl  
# optional part end


cd ThirdParty-HSL
./configure
make
sudo make install
```

### MUMPS
```
git clone https://github.com/coin-or-tools/ThirdParty-Mumps.git
cd ThirdParty-Mumps
./get.Mumps
./configure
make
sudo make install
```

### WSMP
 - Is optional (link below)
 - Check the --with-wsmp option in the next ../configure call below
 - The file wsmp.lic must be present in the directory from which you are 
running a program linked with the WSMP libraries.  You can make multiple 
copies of this file for your own personal use.  Alternatively, you can 
place this file in a fixed location and set the environment variable 
WSMPLICPATH to the path of its location.
 - [Watson Sparse Matrix Package (WSMP)](https://researcher.watson.ibm.com/researcher/view_group.php?id=1426)

## Compile & install
```
git clone https://github.com/coin-or/Ipopt.git
cd Ipopt
mkdir build
cd build

../configure --with-asl=build --with-wsmp="$HOME/source/coin-or/wsmp/wsmp-Linux64-GNU/lib/libwsmp64.a -L$HOME/source/coin-or/wsmp/wsmp-Linux64-GNU/lib64 -lblas -llapack -lpthread -lm -lgfortran" --with-pardiso="$HOME/source/coin-or/libpardiso600-GNU800-X86-64.so -fopenmp -lgfortran"

make
sudo make install
```

## add to ~/.bashrc
```
## parallel version of Pardiso
export OMP_NUM_THREADS=2
## because it couldn't locate usr/local/lib/libipoptamplinterface.so
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
## wsmp
export WSMPLICPATH=$HOME/Source/coin-or/wsmp/wsmp-Linux64-GNU/
```

# SHOT
## requirements
 - cbc-dev: `sudo apt-get install coinor-libcbc-dev`
 - Ipopt: compiled in previous section

## install
```
git clone git@github.com:coin-or/SHOT.git
cd SHOT
mkdir build  
cd build
cmake  -DCMAKE_BUILD_TYPE=Release -DHAS_AMPL=on -DHAS_CBC=on -DHAS_IPOPT=on ../.
cmake --build .
sudo make install
```

# Related Links
| About      | Link |
| ----------- | ----------- |
| Pyomo | https://github.com/Pyomo/pyomo |
| | https://pyomo.readthedocs.io |
| Ipopt      | https://coin-or.github.io/Ipopt/INSTALL.html  |
|     | https://github.com/coin-or/Ipopt  |
| | https://coin-or.github.io/Ipopt/OPTIONS.html#OPT_Warm_Start |
| Metis |  http://glaros.dtc.umn.edu/gkhome/metis/metis/download  |
| HSL | http://hsl.rl.ac.uk/ipopt  |
| WSMP | https://researcher.watson.ibm.com/researcher/view_group_subpage.php?id=7624  |
| SHOT | https://shotsolver.dev/shot/about-shot/compiling  |
|  | https://github.com/coin-or/SHOT  |
| | https://shotsolver.dev/shot/using-shot/solver-options  |
| cmake | https://cmake.org/cmake/help/latest/guide/tutorial/index.html  |
 
