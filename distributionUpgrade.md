# How to release upgrade your distribution

Upgrading from one stable release to the next (e.g. stretch to buster) is done by following the release notes for your architecture. For most people with 32 bit systems that means the Release Notes for Intel x86. For most with 64 bit systems that means the Release Notes for AMD64.

Performing a release upgrade is not without risk. The upgrade may fail, leaving the system in a non-functioning state. USERS SHOULD BACKUP ALL DATA before attempting a release upgrade.

Upgrades are done with package management tools, root or sudo access, and generally involve the following steps.

## 1. Obtain a text shell (tty no WM)

Exit windows manager session -log out, then switch to console tty via `Ctrl+Alt+F[1..7]` 
The upgrade should be done on a text console, because your desktop environment could freeze during post-install/service restarts, leaving the system in a broken state. 
    
## 2. Do the following: 
```bash
# Replace the codename of your release with the next release on apt sources lists and subdirectories
$ sudo vim /etc/apt/sources.list /etc/apt/sources.list.d/*
	
#  Example sources.list from buster to bullseye:
>	deb http://deb.debian.org/debian bullseye main contrib non-free
>	deb-src http://deb.debian.org/debian bullseye main contrib non-free
>	
>	deb http://deb.debian.org/debian bullseye-updates main contrib non-free
>	deb-src http://deb.debian.org/debian bullseye-updates main contrib non-free
>	
>	deb http://security.debian.org/debian-security/ bullseye-security main contrib non-free
>	deb-src http://security.debian.org/debian-security/ bullseye-security main contrib non-free

# clean and update package lists
$ sudo apt clean
$ sudo apt update

# perform regular/minor upgrades
$ sudo apt upgrade     
# perform major release upgrade, removing packages if necessary
$ sudo apt full-upgrade

# remove packages that are not required anymore
$ sudo apt autoremove

# reboot to make changes effective (optional)
$ systemctl reboot
$ /sbin/shutdown -r now
```
## Check
Now check the output of `$ uname -a ; lsb_release -a` and you should see info about the upgraded system. 
