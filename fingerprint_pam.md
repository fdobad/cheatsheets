# Fingerprint authentication

- OS: debian 11
- WM: xfce4
- DM: lightdm
- Hardware: U.are.U 4500 Fingerprint Reader
- Guide: https://wiki.debian.org/SecurityManagement/fingerprint%20authentication

# Install, restart service, enroll your fingerprint
```
# apt install fprintd libpam-fprintd
# sudo systemctl restart fprintd.service

$ fprintd-enroll
$ fprintd-verify
```

# Fingerprint Authentication with sudo
Space to select, enter to confirm
```
# pam-auth-update
```

# Edit your lightdm settings
```
$ sudo lightdm --show-config

   [Seat:*]
A  greeter-session=lightdm-greeter
A  session-wrapper=/etc/X11/Xsession
B  pam-service=lightdm
B  pam-greeter-service=lightdm-greeter

   [LightDM]
B  dbus-service=true

Sources:
A  /usr/share/lightdm/lightdm.conf.d/01_debian.conf
B  /etc/lightdm/lightdm.conf
```

