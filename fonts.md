# X11
 - Default i3-sensible-terminal ~ mlterm sucks:
    ```
    # apt install xterm xfonts-utils x11-xfs-utils 
    # apt install fonts-terminus
    # apt install xfonts-100dpi fonts-liberation freetype
    ```

 - Default fonts directory 
      X11 : `/etc/X11/fonts` look for alias files
      freetype? : `/usr/share/fonts/`

 - User session config `~/.Xresources`, example
    ```
    xterm*faceName: Noto Mono Regular
    xterm*faceSize: 11
    ```

## commands
```
# Get screen dpi
$ xdpyinfo | grep resolution

# Redo font lists
$ dpkg-reconfigure fontconfig fontconfig-config

# List monospaced fonts
$ fc-list | grep ono
$ find /usr/share/fonts -name *Mono*
```

### mmmph
xrandr --dpi 120


## xterm
 - FreeType triumphs X11
   ` xterm -fa FreeType > -fn X11 `
 - Select from menu
   ` xterm -fc `

# References
https://wiki.debian.org/Fonts
