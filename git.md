# GIT  
## archive

    git archive --format=zip --prefix=folder/other_pre/ -o file_name.zip HEAD|commit-id
    git archive --format=zip --prefix=prefix_name_for_all_files -o file_name.zip 7-short-commit-id

## tags
```
# list tags
git tag

# tag current branch-commit
git tag -a v0.3Name -m 'message'

# delete 
## local
git tag --delete tagname
## remote
git push --delete origin tagname

# push 
## a tag
git push origin v0.3Name
## all tags
git push origin --tags
```
### github make release from tag
```
# touch .github/workflows/main.yml with content:
name: Create Archive

on:
  create:
    tags:
      - v*

permissions:
  contents: write

jobs:
  build-n-publish:
    runs-on: ubuntu-latest

    steps:
    - name: Checkout
      uses: actions/checkout@master

    - name: Zip Release
      uses: TheDoctor0/zip-release@0.7.1
      with:
        type: 'zip'
        filename: 'fire2am.zip'
        exclusions: '*.git*'

    - name: Create Release
      uses: ncipollo/release-action@v1.12.0
      with:
        artifacts: 'fire2am.zip'
        token: ${{ secrets.GITHUB_TOKEN }}

```

## undo
```
git checkout .
git reset
git clean -fd
```
## from stash
```
git stash list
git stash apply
```
[ref](https://riptutorial.com/git/example/4906/recover-from-git-stash)

## Sparse checkout
```
function git_sparse_clone() {
  rurl="$1" localdir="$2" && shift 2

  mkdir -p "$localdir"
  cd "$localdir"

  git init
  git remote add -f origin "$rurl"

  git config core.sparseCheckout true

  # Loops over remaining args
  for i; do
    echo "$i" >> .git/info/sparse-checkout
  done

  git pull --depth=1 origin master
}
```

## push branch to remote  
[ref](https://devconnected.com/how-to-push-git-branch-to-remote/)
```
# push branch to remote
cd github-services  
git remote add upstream git@github.com:...  
git fetch upstream  
git merge upstream/master master  
git rebase upstream/master...  

# == git fetch + git merge  
git pull upstream master 
```

## Create from scratch
```
git init -b main
git add  .  
git commit -m "initial commit"  
git remote add origin git@github.com:fdobad/cheatsheets.git  
git push -u origin master  
```  

## add 2 tracking branches
git init -b main
git branch

## Check changes before commit
```  
git status  
git diff file  
```  

## merge 
https://gist.github.com/fdobad/819b0ae41b6c2d6467843519b10406c7

    # probably want the latests from each branch
    git switch main && git pull
    git switch feature && git pull

    # switch _to_ main, choose where to merge _from_
    git checkout main
    git merge <feature_branch|commit_id>
    
    # clone current branch to feature, merge from commit_id
    git merge -b feature commit_id

    # solve conflicts
    git mergetool

    # navigate
    ]c          " next difference
    [c          " previous difference

    # mappings for choosing left, center or right column
    if &diff
      map <leader>1 :diffget LOCAL<CR>
      map <leader>2 :diffget BASE<CR>
      map <leader>3 :diffget REMOTE<CR>
    endif

    # after cleanup delete
    git clean -n *.orig
    git clean -f *.orig

### flow v2.0

    git clone -b development <development repository>  <devdir>; cd <devdir>
    git checkout -b new-branch
    #<edit, build test>
    git add ...
    git commit ...
    #repeat above 3
    
    #now you want to get your changes back on development
    git pull origin development
    #<build, test - to ensure new-branch works w/ latest development>
    git push origin new-branch:development

## Branches
```
# create new branch
git checkout -b NewBranch

# set tracking branch
git checkout master

# list all, local, remote
git branch -a -l -r

# show tracking urls configs
git remote show origin

```
## Remotes
```
# show
git remote -v  

# add
git remote add branchName git@github.com:userName/repoName.git  

# change tracking
git remote set-url origin git@gith

```

## Submodules
https://git-scm.com/book/en/v2/Git-Tools-Submodules

### Init

    git submodule init  
    git submodule add --depth 1 --name tagbar git@github.com:majutsushi/tagbar.git pack/plugin/start/tagbar  

### clone repos with Submodules

    git clone --recurse-submodules git@...    # clone with submodules

    git submodule update --init --recursive   # forgot to clone with submodules
    git submodule update --recursive --remote # update all submodules

    git submodule update --init # choose which one
    git submodule update --init -- path/to/specific/submodule

### Update a repo with Submodules with changes
```
git submodule foreach git pull --rebase -f origin master  
? git submodule update --remote --merge ?  
? git commit ?  
```

### switch branches with submodules

    git checkout --recurse-submodules master

### Deinit 
```
# Remove the submodule entry from .git/config
git submodule deinit -f path/to/submodule

# Remove the submodule directory from the superproject's .git/modules directory
rm -rf .git/modules/path/to/submodule

# Remove the entry in .gitmodules and remove the submodule directory located at path/to/submodule
git rm -f path/to/submodule
```

## shell like commands
```
git mv oldfolder newfolder : flags -f force, -n dryrun  
git add -u newfolder  
git commit -m "changed the foldername whaddup"  
```

## gitconfig
```
git config --list --show-origin
```
minimal
```
[user]
        name = fdobadeidos
        mail = fernando.badilla@eidosanalytics.com
[core]
        editor = gvim
```
