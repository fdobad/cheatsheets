# gnome 3 terminal cmd settings

## window borders
```
gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'dark'
gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'light'
gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'system'
```

## profiles
```
# list all scheme
gsettings list-recursively org.gnome.Terminal.ProfilesList

# uuid
gsettings get org.gnome.Terminal.ProfilesList default
    '898a02ac-0a18-4c7c-8838-26303a23d458'

gsettings get org.gnome.Terminal.ProfilesList list
    # dark light system <-- set manually with solarized
    ['b1dcc9dd-5262-4d8d-a863-c897e6d979b9', 'c98847a3-3967-45cc-b1d7-7646f2e945ae', '898a02ac-0a18-4c7c-8838-26303a23d458']

# get config for a profile
gsettings list-recursively "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/">/tmp/dark
gsettings list-recursively "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:c98847a3-3967-45cc-b1d7-7646f2e945ae/">/tmp/light
gsettings list-recursively "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:898a02ac-0a18-4c7c-8838-26303a23d458/">/tmp/system
gsettings list-recursively "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:898a02ac-0a18-4c7c-8838-26303a23d458/">/tmp/system_dark
gsettings list-recursively "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:898a02ac-0a18-4c7c-8838-26303a23d458/">/tmp/system_light
```

# differences
## system light
```
org.gnome.Terminal.Legacy.Profile foreground-color '#171421'
org.gnome.Terminal.Legacy.Profile palette ['rgb(23,20,33)', 'rgb(192,28,40)', 'rgb(38,162,105)', 'rgb(162,115,76)', 'rgb(18,72,139)', 'rgb(163,71,186)', 'rgb(42,161,179)', 'rgb(208,207,204)', 'rgb(94,92,100)', 'rgb(246,97,81)', 'rgb(51,209,122)', 'rgb(233,173,12)', 'rgb(42,123,222)', 'rgb(192,97,203)', 'rgb(51,199,222)', 'rgb(255,255,255)']
org.gnome.Terminal.Legacy.Profile visible-name 'system'
org.gnome.Terminal.Legacy.Profile use-theme-colors true
org.gnome.Terminal.Legacy.Profile background-color '#ffffff'
```
## dark
```
org.gnome.Terminal.Legacy.Profile foreground-color 'rgb(101,123,131)'
org.gnome.Terminal.Legacy.Profile palette ['rgb(7,54,66)', 'rgb(220,50,47)', 'rgb(133,153,0)', 'rgb(181,137,0)', 'rgb(38,139,210)', 'rgb(211,54,130)', 'rgb(42,161,152)', 'rgb(238,232,213)', 'rgb(0,43,54)', 'rgb(203,75,22)', 'rgb(88,110,117)', 'rgb(101,123,131)', 'rgb(131,148,150)', 'rgb(108,113,196)', 'rgb(147,161,161)', 'rgb(253,246,227)']
org.gnome.Terminal.Legacy.Profile visible-name 'light'
org.gnome.Terminal.Legacy.Profile use-theme-colors false
org.gnome.Terminal.Legacy.Profile background-color 'rgb(253,246,227)'
```
## light
```
org.gnome.Terminal.Legacy.Profile foreground-color 'rgb(131,148,150)'
org.gnome.Terminal.Legacy.Profile palette ['rgb(7,54,66)', 'rgb(220,50,47)', 'rgb(133,153,0)', 'rgb(181,137,0)', 'rgb(38,139,210)', 'rgb(211,54,130)', 'rgb(42,161,152)', 'rgb(238,232,213)', 'rgb(0,43,54)', 'rgb(203,75,22)', 'rgb(88,110,117)', 'rgb(101,123,131)', 'rgb(131,148,150)', 'rgb(108,113,196)', 'rgb(147,161,161)', 'rgb(253,246,227)']
org.gnome.Terminal.Legacy.Profile visible-name 'dark'
org.gnome.Terminal.Legacy.Profile use-theme-colors false
org.gnome.Terminal.Legacy.Profile background-color 'rgb(0,43,54)'
```
