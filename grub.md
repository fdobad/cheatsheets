# references

https://www.devdungeon.com/content/change-grub-bootloader-colors-image-timeout

# start-finish
sudo -e /etc/default/grub.cfg
...
sudo update-grub

# set font size

SCREEN_WIDTH=$(xdpyinfo | grep dimensions | cut -d ':' -f 2 | cut -d 'x' -f 1)
FONT_SIZE=$((${SCREEN_WIDTH} / 80))

sudo grub-mkfont -s ${FONT_SIZE} -o /boot/grub/fonts/DejaVuSansMono.pf2 /usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf

sudo grub-mkfont -s ${FONT_SIZE} -o /boot/grub/fonts/LiberationMono.pf2 /usr/share/fonts/truetype/liberation/LiberationMono.ttf

printf "\nGRUB_FONT=/boot/grub/font/DejaVuSansMono.pf2" | sudo tee -a /etc/default/grub
sudo grub-mkconfig -o /boot/grub/grub.cfg

# set background image

GRUB_GFXMODE=3200x1800
GRUB_BACKGROUND=/boot/grub/path-to-image.png

# menu colors

GRUB_COLOR_NORMAL="light-green/green"
GRUB_COLOR_HIGHLIGHT="light-gray/white"

GRUB_COLOR_NORMAL="light-blue/black"
GRUB_COLOR_HIGHLIGHT="light-green/blue"

    black
    blue
    green
    cyan
    red
    magenta
    brown
    light-gray

    These below can be specified only for the foreground.
    dark-gray
    light-blue
    light-green
    light-cyan
    light-red
    light-magenta
    yellow
    white 
