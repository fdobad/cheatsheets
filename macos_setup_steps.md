MACOS CLEAN INSTALL

# Homebrew
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"  
	$ brew install git vim tmux fzf ctags gcc 
	??brew link python@3.8??

# fzf
	$ /usr/local/var/homebrew/linked/fzf/install --all 
	$ echo $'"FZFhomebrew\nset rtp+=/usr/local/opt/fzf\n'>>.vimrc 

# conda jupyterlab
	download https://docs.conda.io/en/latest/miniconda.html  
	$ chmod a+x Miniconda3-latest-MacOSX-x86_64.sh  
	$ ./Miniconda3-latest-MacOSX-x86_64.sh  
	$ conda create -n jupyter_env jupyterlab qtconsole matplotlib numpy pandas nodejs  
	$ conda activate jupyter_env  
	$ echo "conda activate jupyter_env">>.bash_profile  
	$ conda install -c conda-forge jupyterlab-git ipympl cython  
	$ jupyter labextension install @jupyter-widgets/jupyterlab-manager @jupyterlab/git @jupyterlab/toc jupyter-matplotlib qgrid2  
	$ jupyter lab build  

# pip jupyterlab
	$ pip3 install virtualenv
	$ python3 -m venv Source/py3env
	$ source Source/py3env/bin/activate
	$ pip list --outdated
	$ pip

## \%matplotlib widget not working
Installation:  
Using conda  

conda install -c conda-forge ipympl  
### If using the Notebook  
conda install -c conda-forge widgetsnbextension  
### If using JupyterLab  
conda install nodejs  
jupyter labextension install @jupyter-widgets/jupyterlab-manager  
jupyter labextension install jupyter-matplotlib  

Using pip  

pip install ipympl  
### If using JupyterLab  
conda install nodejs  
jupyter labextension install @jupyter-widgets/jupyterlab-manager  
jupyter labextension install jupyter-matplotlib  

# Launch on startup : This doesn't work
	mkdir -p Library/LaunchAgents  
	cp macos/conda-jupyter-startup.sh Library/LaunchAgents/. 
	cp macos/jupyter.plist Library/LaunchAgents/.  

## Load on startup
https://www.jacobtomlinson.co.uk/posts/2019/how-to-run-jupyter-lab-at-startup-on-macos/
## test it
launchctl unload ~/Library/LaunchAgents/jupyter.plist

# about this mac
Os 10.13.6 (17G13035)  
MacBook Air  
1,4 GHz Intel Core 2 Duo  
NVIDIA GeForce 320M 256 MB  
