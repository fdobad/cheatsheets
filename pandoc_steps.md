pandoc
Export jupyterlab notebook to pdf or html

# steps 
pip install pandoc
sudo apt install pandoc
sudo apt-get install texlive-xetex texlive-fonts-recommended texlive-generic-recommended

# sources
https://nbconvert.readthedocs.io/en/latest/install.html#installing-tex
https://pandoc.org/installing.html
https://github.com/jgm/pandoc/releases/tag/2.13
