# pyenv
multiple local pythons
https://github.com/pyenv/pyenv

## list & install 
    pyenv install -l
    pyenv install 3.10.4

## set `PYENV_VERSION`
    pyenv shell

## switch
    pyenv shell <version> -- select just for current shell session
    pyenv local <version> -- automatically select whenever you are in the current directory (or its subdirectories)
    pyenv global <version> -- select globally for your user account

## unswitch
Using "system" as a version name would reset 

## set app specific folder specific `.python-version` (searchs outwards)
    pyenv local

## $(pyenv root)/version
### automatically resolve full prefixes to the latest version
    pyenv global 3.10



### which real executable would be run when you invoke <command>
    pyenv which <command>

## dev install
    git clone https://github.com/pyenv/pyenv.git ~/.pyenv
    cd ~/.pyenv && src/configure && make -C src

    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
    echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(pyenv init -)"' >> ~/.bashrc

    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile
    echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.profile
    echo 'eval "$(pyenv init -)"' >> ~/.profile

    exec "$SHELL"

    sudo apt update; sudo apt install build-essential libssl-dev zlib1g-dev \
    libbz2-dev libreadline-dev libsqlite3-dev curl \
    libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

## cplex install
    pyenv install 3.10.4
    pyenv global 3.10
    mkdir pyvenv
    python -m venv pyvenv/cplex
    source ~/pyvenv/cplex/bin/activate
    python -m pip install --upgrade pip wheel
    cd /opt/ibm/ILOG/CPLEX_Studio2211
    sudo chmod -R a+w python # <- optional?
    cd cplex
    sudo chmod -R a+w python

