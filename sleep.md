
__Sleep management__

# Check system capabilities
## kernel
cd /sys/power
cat state (supported sleep states)
	disk	:	hibernation
	freeze	:	suspend-to-idle
	standby	:	standby
	mem		: 	check mem_sleep
cat mem_sleep
	s2idle	:	suspend-to-idle
	shallow	:	standby 
	deep	:	suspend-to-RAM
cat disk (mode of hibernation or suspend to disk)
	suspend	:	Hybrid system suspend
	platform:	S4
	shutdown
	reboot
	test_resume

## rtcwake
rtcwake --list-modes
	standby	:	S1
	freeze	:	saves less than suspend2ram
	mem		:	S3 suspend2ram
	disk	:	S4 suspend2disk
	off		:	S5 poweroff

# Test
## MacBookPro5,1 debian 11
### capabilities
- cat
	state		:	freeze mem disk
	mem_sleep	:	s2idle [deep]
	disk		:	[platform] shutdown reboot suspend test_resume
- rtcwake
	freeze mem disk off no on disable show

### tests
1. /etc/login.con /etc/... 


# References
- https://01.org/linuxgraphics/gfx-docs/drm/admin-guide/pm/sleep-states.html
- https://www.freedesktop.org/software/systemd/man/systemd-sleep.conf.html
- https://www.freedesktop.org/software/systemd/man/logind.conf.html
- https://unix.stackexchange.com/questions/50748/how-can-i-set-power-button-on-computer-case-to-power-off-system-with-systemd
- https://linuxcommandlibrary.com/man/rtcwake#description
