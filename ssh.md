# ssh

## connect to server
```
ssh -X user@serverIP:~/directory
# -X enables X11 server forwarding

# copy files
scp user@serverIP:~/directory/file /local/path/.
```

### store public key on server
on `~/.ssh/authorized_keys`
```
user@client:$ ssh-copy-id user@server
```

## setup a server
To connect without password but public key
```
#1 install
sudo apt-get install openssh-server

#2 config security 
sudo vim /etc/ssh/sshd_config

> LogLevel VERBOSE
> PermitRootLogin forced-commands-only
> PubkeyAuthentication yes
> PasswordAuthentication no
> AllowUsers <username1> <username2>
> AllowAgentForwarding yes
> X11Forwarding yes

#3 paste authorized public keys
sudo vim ~/.ssh/authorized keys
```

### control via systemctl 
```
sudo systemctl enable sshd
sudo systemctl status sshd
sudo systemctl stop sshd
sudo systemctl restart sshd
```

### check whether your service is enable or not, you can run the following command
```
sudo systemctl list-unit-files | grep enabled | grep ssh
```

### who is connected?
```
who -a
```
[ref](https://devconnected.com/how-to-install-and-enable-ssh-server-on-debian-10/)

## create public/private key pair
```
ssh-keygen -t rsa -b 4096 -C "RegisteredMail@GitLab"  
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
cat ~/.ssh/id_rsa.pub then upload it to https://github.com/settings/keys or https://gitlab.com/-/profile/keys
```
You can reuse the key pair in different machines/installs but won't know who made the commit

## paranoidly reset ssh files permissions
```
cd ~
find .ssh/ -type f -exec chmod 600 {} \;
find .ssh/ -type d -exec chmod 700 {} \;
find .ssh/ -type f -name "*.pub" -exec chmod 644 {} \;
chmod 400 ~/.ssh/id_rsa
```
[ref](https://superuser.com/questions/215504/permissions-on-private-key-in-ssh-folder)
