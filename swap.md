# Swap 
## Make
 - Make with gparted
   - Size 2x ram for suspend or hibernate
   - Name is external for GPT
   - Label is internal for fs
 - Persist by adding `UUID="" none swap sw 0 0"` to fstab
   - UUID could be replaced by LABEL
 - Reconfigure initramfs
	```
	$ sudo blkid
	$ sudo -e /etc/fstab
	$ sudo apt install --reinstall initramfs-tools
	```
## enable on the fly
```
sudo echo "LABEL=swap none swap sw 0 0">>/etc/fstab
sudo swapon --all --verbose
```

## Check
```
$ cat /proc/swaps

$ free -h  

$ sudo swapon

#Get UUID
$ sudo blkid  

#Change labels  
$ e2label 

#Change partition name
$ gdisk

# View disks by- uuid, label, partlabel, 
$ ls /dev/disk/by-*

```

