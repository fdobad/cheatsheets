__systemd__

# timer
```
systemctl stop yeah.timer
```

# disable shit for resources
```
apt-get -y purge update-notifier-common ubuntu-release-upgrader-core landscape-common unattended-upgrades

systemctl kill --kill-who=all apt-daily.service
systemctl kill --kill-who=all apt-daily-upgrade.service
```

# sleep
```
[Sleep]
SuspendState=freeze
```
# check
```
systemctl start|stop|restart|reload|status <name>.service
mask|unmask?
systemd-analyze
systemd-analyze blame
systemctl list-unit-files
systemctl list-units
systemctl –failed
```
