# .tmux.conf
'''
set-option -g prefix C-a
set -g mouse on
# needed by vim-clipboard
set -g focus-events on
# address vim mode switching delay (http://superuser.com/a/252717/65504)
set -s escape-time 0

# split vertical & horizontal pane
# Open new panes/windows in the same directory
bind -T prefix 'v' split-window -c "#{pane_current_path}"
bind -T prefix 'b' split-window -h -c "#{pane_current_path}"
bind -T prefix 'c' new-window -c "#{pane_current_path}"

# copy mode vim
set -g mode-keys vi
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'C-v' send-keys -X rectangle-toggle
bind-key -T copy-mode-vi 'y' send -X copy-selection-and-cancel
# prefix # : see buffers
# prefix = : select and paste buffer

# navigate vim mode panes
bind -T prefix h select-pane -L
bind -T prefix l select-pane -R
bind -T prefix k select-pane -U
bind -T prefix j select-pane -D
# prefix n : next window
# prefix p : prev window
'''

# .vimrc for copy/paste between vim/tmux
## 1 pasting vim buffer to a tmp file, pasting buffer on last pane
Advantages no plugins
'''
"" Sets the default clipboard to the system clipboard
if has('clipboard')
	set clipboard=unnamed
	if has('unnamedplus')
		set clipboard=unnamedplus,autoselect,exclude:cons\|linux " Sets the default clipboard to the system clipboard
	endif
endif

vmap <leader>; :w! ~/vim.tmp<CR> :!tmux set-buffer "$(cat ~/vim.tmp)"<CR> :execute '!tmux select-pane -l \; paste-buffer -p \; send-keys Enter Enter \; select-pane -l'<CR><CR>
'''

## 2 Using vim-tmux-clipboard plugin
Advantage shares same clipboard across vim sessions under same tmux session
1. Install vim-tmux-clipboard plugin
'''
mkdir -p ~/.vim/pack/plugin/start
cd ~/.vim/pack/plugin/start
git clone https://github.com/roxma/vim-tmux-clipboard
'''
2. on .vimrc
'''
vmap  <leader>; y :execute('!tmux paste-buffer -p -t {last} \; send-keys -t {last} Enter Enter') <CR><CR>
'''
