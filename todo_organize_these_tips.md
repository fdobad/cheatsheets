# Without DM, leave the terminal closed on logout
```bash
# .bash_profile
	startx; exit;

# .xinitrc
	exec i3; exit;
```

# DEBIAN BUSTER CLEAN INSTALL

# **apt**
```bash
sudo apt-add-repository non-free
sudo apt update
sudo apt install firmware-brcm80211 firmware-misc-nonfree
sudo apt upgrade
```  
## Ammenities
```bash
sudo apt install vlc
```
##  Base business: _vim+python3+jupyterlab_
```bash
sudo apt remove vim-tiny
sudo apt install bash-completion tmux vim-nox python3 python3-dev python3-venv ipython3 git exuberant-ctags fzf nodejs npm
```
# **Git** setup
```bash
git config --global user.name "fdobad" 
git config --global user.mail "1234567-fdobad@users.noreply.gitlab.com"
git config --global core.editor vim
```
## Create & upload **ssh key**
```bash
git config --global user.name "fdobad" 
ssh-keygen -t rsa -b 4096 -C "YourReal@Github.RegisteredMail"
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
```  
Add public key into github `https://github.com/settings/keys`:
```bash
cat ~/.ssh/id_rsa.pub
```
# **Python** virtual environment
## Create
Create a new env. named `py3env`, located in `~/Source`:  
```bash
mkdir -p ~/Source/py3env
python3 -m venv ~/Source/py3env
source ~/Source/py3env/bin/activate
pip3 install --upgrade pip
pip3 install pylama
```  
## Startup
Permanently load on start-up:  
```bash
echo "source ~/Source/py3env/bin/activate">>~/.bashrc
```

## **jupyter-lab** install
```bash
pip3 install wheel jupyter jupyterlab jupyterlab-git matplotlib numpy pandas ipywidgets qgrid2 ipympl Cython PySide2|pyqt5
jupyter labextension install @jupyter-widgets/jupyterlab-manager @jupyterlab/toc @jupyterlab/git jupyter-matplotlib qgrid2
jupyter lab build
```
### qt console, normal not working
```bash
#jupyter-console --generate-config
jupyter-qtconsole --generate-config
#sed -i 's/SEARCH_REGEX/REPLACEMENT/g' INPUTFILE
sed -i 's/#c.ConsoleWidget.include_other_output = False/c.ConsoleWidget.include_other_output = True/' /home/fdo/.jupyter/jupyter_qtconsole_config.py
```

## Update pip
Update 1 by 1 to identify things breaking!
```bash
pip3 list --outdated
pip3 install --upgrade PackageName
```

# **Vim**
## From git repo
```bash
git clone git@github.com:fdobad/vim-plugins.git ~/.vim/pack/plugin
mv ~/.vim/pack/plugin/.vimrc ~/. # need a vimrc?
:helptags ALL # to update help documentation from plug-ins
``` 
## From scratch
Create minimal vimrc
```bash
vim ~/.vimrc
```
>syntax on  
>filetype plugin indent on  
>set tabstop=4

Create your own repo on
```bash
mkdir -p ~/.vim/pack/plugin/start ~/.vim/pack/plugin/opt
cd ~/.vim/pack/plugin
git init
git submodule init
git submodule add --depth 1 https://github.com/majutsushi/tagbar start/tagbar
git submodule add --depth 1 git@github.com:jupyter-vim/jupyter-vim.git opt/jupyter-vim
# more plugins, etc...
vim .gitignore
```
>\_\_pycache__/  
>*.py[cod]  
>*\$py.class  
>.ipynb_checkpoints/  
>*.swp  
>*~   
```bash
git add .
git commit -m "initial commit"
git remote add origin git@github.com:fdobad/vim-plugins.git
git push -u origin master
```
## Update plug ins
Now you can update all plugins using
```bash
~/.vim/pack/plugin$ git submodule foreach git pull -f origin master
# or?
git submodule update --remote --merge  
git commit
```
## Delete plug in
```bash
git submodule deinit start/vim-airline
git rm start/vim-airline
rm -Rf .git/modules/start/vim-airline
git commit
```
## Clean python stuff
```bash
cd ~/.vim/pack/plugin/start/python-mode
find . -type f -name '*.pyc' -delete
find . -type f -name '*.__pycache__' -delete
rm -r __pycache__/

```
# ~~Voilà~~
Asyncrun https://github.com/skywind3000/asyncrun.vim  
apt install fzf != ~/.fzf/install

# Coming from **win10**?

## First install

1. Update efi/bios
2. \<F12> to enter BIOS
 - Change "RAID On" to "AHCI"
 - Disable Secure Boot
3. Burn usb live disk
4. Don't enable non-free-repos, share internet from Android usb
5. install wifi from...

## Windows 10 update messed my dualboot grub

1. Boot into Windows 10
2. Open the Command Prompt (in Administrator Mode).
3. Go to Start -> Command Prompt -> Right Click and "Run as Administrator".  
Or Super+cmd click on "Run as Administrator"
4. Type in the following command and press enter:

```
bcdedit /set {bootmgr} path \EFI\debian\grubx64.efi
```

5. Now Restart your system, and the GRUB menu should be loaded. Select the OS you want to boot into.

## Install windows linux subsystem wsl 2
Install debian in windows linux subsystem v2  
From: https://docs.microsoft.com/en-us/windows/wsl/install-win10  

1. Open PowerShell as Administrator and run:  
```
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
```
Restart your machine.  
2. Open PowerShell as Administrator and run:  
```
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```
Restart your machine.  
3. Run the following command in PowerShell to set WSL 2 as the default version when installing a new Linux distribution:
```
wsl --set-default-version 2
wsl --list --verbose
```  

4. Update to its kernel component, download & install from: https://aka.ms/wsl2kernel  

5. Install debian from the store:
https://www.microsoft.com/store/apps/9MSVKQC78PK6

6. Add to executable to the firewall `C:\Windows\System32\wsl.exe`

0. ~~Remove all ~~?
```
lxrun /uninstall /full 
wsl --unregister Legacy
```
