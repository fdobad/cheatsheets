# vim cheatsheet

## Lastest tricks

     :edit file1 | vert diffsplit file2

## References

- smart: https://learnvim.irian.to/
- hard: https://learnvimscriptthehardway.stevelosh.com/  
- https://vim.fandom.com/wiki/Vim_Tips_Wiki  
- https://vimsheet.com/  

## useful variables

    :echo @%                |" directory/name of file
    :echo expand('%:t')     |" name of file ('tail')
    :echo expand('%:p')     |" full path
    :echo expand('%:p:h')   |" directory containing file ('head')
    :cd %:p:h               |" cd to current file directory

## regex

    :%s/\s\+$//e                |" delete white trailspace
    :g/.\*word.\*/s/.\*\n//c    |" delete lines with word
    :%s/.\*word.\*\n//gc        |" delete lines with word
    :g/.\*word.\*/normal dd     |" finds all instances of word and then executes the normal command after it
    :%s/.\*word.\*\n//gc        |" same but with confirmation

## marks

    (visual) m<lower_case_letter>    |" mark position
    '<lower_case_letter>             |" go back to marked line
    `<lower_case_letter>             |" go back to mark
    <C-o> and <C-i>                  |" Cycle-jump between last positions

## navigate
### lines

    <C-d>      |" half page down
    <C-u>      |" half page up

    gg         |" go to first line
    G          |" go to last line
    33G        |" go t line 33
    :33<Enter> |" 
    ggVG       |" visual select all buffer 
    gg=G       |" Correct full buffer indentation

### Fzf floating windows

    <Tab>      |" multiple select
    <Enter>    |" open selected in current window
    <C-t>      |"      in new tab
    <C-x>      |"      in horizontal splits
    <C-v>      |"      in vertical splits

## copy cut paste 
### terminal cheats

    Shift+Delete     |" Cut text and copy it in the clipboard
    Ctrl+Insert	     |" Copy text in the clipboard
    Shift+Insert	 |" Paste text from the clipboard into a document

    Shift+MouseClick |" Start terminal select text
    Ctrl+Shift+C     |" Copy
    Ctrl+Shift+v     |" Paste 


### registers
There are 3 clipboard sources in vim: internal, system `"+` and visual selection `"*`.  

    :reg    |" see internal clipboard "history"  
    "*      |" visual select register or mouse button 3  
    "+      |" system clipboard or `ctrl+c`  

### simple

    v       |" start visual select
    <C-v>   |" toggle squared select
    x       |" cut
    y       |" copy
    yy      |" copy current line
    d       |" delete
    dd      |" delete current line
    p       |" paste over
    P       |" paste insert

    u       |" undo
    <C-R>   |" redo

### with motions
In visual mode standing inside the text object

    [verb][inner|outer][text object]
    di( |" delete inside parenthesis ex. ('hola') -> ()
    ca[ |" cut outer brackets [1,2,3] -> 

## special chars
:digraphs  
- Greek type: `<C-k>a\*` ~ alpha  
- Emoji `<C-v>U1F60A<esc>` ~ smiley-face

# regex
| command | does |
| - | - | 
| :%s/\s\+$//e | remove all trailing whitespace | 
| :g/.\*text.\*/s/.\*\n//c | delete lines with text |
| :%s/.\*text.\*\n//gc | delete lines with text w/confirmation |
| :g/.\*word.\*/normal dd | finds all instances of word and then executes the normal command after it | 

# special chars
- list all : `:digraphs`
- Greek type : `<C-k>a\*` ~ alpha  
- Emoji : `<C-v>U1F60A<esc>` ~ smiley-face
- https://unicode.org/emoji/charts/full-emoji-list.html

| key | cmd  |
| - | - |
| v | start horizontal select |
| `<C-v>` | start squared select |
| x | cut |
| y | copy |
| d | delete |
| p | paste over |
| P | paste insert |
| u | undo |
| `<C-R>` | redo |

## copy paste & registers
There are 3 clipboard sources in vim: internal, system `"+` and visual selection `"*`.  
- `:reg` see internal clipboard "history"  
- `"*` visual select register or mouse button 3  
- `"+` system clipboard or `ctrl+c`  

| key | cmd  |
| - | - |
| v | start horizontal select |
| `<C-v>` | start squared select |
| x | cut |
| y | copy |
| d | delete |
| p | paste over |
| P | paste insert |
| u | undo |
| `<C-R>` | redo |

# Grep
|   |   |
| - | - | 
| __:vimgrep__ | [Find within files](https://vim.fandom.com/wiki/Find_in_files_within_Vim#Recursive_Search) |
| - what to ignore | `:set wildignore+=\*pyc,__pycache__` |
| - only tracked files | `:noautocmd vimgrep /{word_pattern}/gj "git ls-files" ` |
| - search current word, open quicklist (vimrc) | `map <F4> :execute "vimgrep /" . expand("<cword>") . "/j **" <Bar> cw<CR>` |
| all ocurrences within buffer | `:vimgrep /pattern/ %` |
| __grep__ from shell | `$ grep -n -R {word_pattern} {filepath_pattern} --exclude-dir={dir_pattern}` |

# Buffers, windows, tabs
|   |   |
| - | - |
| `:set hidden` | enable hidden buffers |
| `:qa!` | beware! discard unsaved changes that you don't see |
| `$ vim file1 .. file8` | open into buffers |  
| `$ vim -O\|o ${fzf +m}` | open into vert\|splits |
| `$ vim -p file1 .. file8` | open into tabs |
| `:ls` | list buffers |
| `:e {filename}` | add not show |
| `:v\|split or <C-W> v\|s`| add splitted window repeating buffer |
| `:buffer #` | switch by number |
| `:b name` | switch by name |
| `:bn` | switch to next buffer in list |
| `:tabe {filename}` | add\|show in new tab |
| `:tabnew` | create empty tab |

# Sessions
|   |   |
| - | - |
| ` :mks[!] .vim/sessions/rooster.vim ` | Save !overwrite session |
| ` $ vim -S .vim/sessions/rooster.vim ` | Open session |

# Quick list
| TBD  |   |
| - | - |
| `<CR>`      | open the file in the quicklist in new split(buffer) | 
| :copen      | :lopen |
| :cnext      | :lnext |
| :cprev      | :prev |
| :cc         | :ll |
| :cf \<file> | :lf \<file> |

## Diff

    :Gvdiff HEAD~2      # vertical split file with two commits before
                        # or use a commit id

	$ vimdiff == vim -d
	:diff\
		s|plit in new window					:diffs|plit {filename}
		t|his here add to diff windows <8 		:difft|his
		p|atch w/ open a buffer with the result	:diffp|atch {filename}
			patch not working in win10

	:vert diffs|t|p...

	:diffo|ff|! switch off for current window |for current tab!
                ?twice restores last saved values?

	:diffopt vertical foldcolumn 4
```

# Navigation
|   |   |
| - | - |
| ` <nmode> gt\  |T `  | Jump Next\|Previous tab |
| `:nnoremap <C-J> <C-w><C-J> `| Jump between splits without w (vimrc) |
| `<C-w> + or -` | height resize window |
| `<C-w> _ `     | current to max height |
| `<C-w> < or >` | width resize window |
| `<C-w> \|`     | current to max width |
| `<C-w> =`      | all windows equal |
| `z#<CR>`       | set windows height to # |

# PlugIns
## Installing >v8.0
Just put the files in `~/vim/pack/plugin/start|opt`  
They get loaded on startup|command(`:pa plugInName`)
## Mantaining example
### Init
```
    mkdir -p .vim/pack/plugin  
    cd .vim/pack/plugin  
    git init  
    git submodule init  
    git submodule add --depth 1 git@github.com:majutsushi/tagbar.git start/tagbar  
    git submodule add --depth 1 git@github.com:junegunn/fzf.vim.git start/fzf  
    git submodule add --depth 1 git@github.com:roxma/vim-tmux-clipboard.git start/vim-tmux-clipboard
    git submodule add --depth 1 git@github.com:fdobad/vim-repl.git opt/vim-repl
    git submodule add --depth 1 git@github.com:jupyter-vim/jupyter-vim.git opt/jupyter-vim
```
### Update
```
    git submodule foreach git pull -f origin master  
    git submodule update --remote --merge ?  
```
### rm submodule
```
  git submodule deinit -f /path/to/submodule
  rm -rf .git/modules/path/to/submodule
  git commit ?
  git rm -f path/to/submodule
```
#### jupyter-vim wtf!
[submodule "start/jupyter-vim"]
  path = start/jupyter-vim
  url = git@github.com:jupyter-vim/jupyter-vim.git
        ```
        Error detected while processing function jupyter#init_python[4]..<SNR>26_init_python:  
        line   20:  
        E605: Exception not caught: [jupyter-vim] s:init_python: failed to run Python for initialization: Vim(pythonx):Traceback (most recent call last):.  
        Error detected while processing /home/fdo/.vim/pack/plugin/start/jupyter-vim/plugin/jupyter.vim:  
        line   14:  
        E171: Missing :endif  
        ```
# Installing for python
## From
- windows(32bit) : https://github.com/vim/vim-win32-installer/releases  
- debian: `sudo apt install tmux vim-gtk fzf ripgrep universal-ctags xclip`
## Check version and architecture  
- `$ vim --version` should have: +python3 and  
    - windows: python##.dll  
    - debian: -L/usr/lib/python3.7/config-3.7m-x86_64-linux-gnu  
- `:py3 import platform;print(platform.architecture())`  
- `:py3 import sys;print(sys.version)`  
## Using python virtual environment?
- activate : `source ...bin/activate ` then run
- check which python : `:py3 import sys;print(sys.prefix)`  

# Troubleshoot
See what's loaded:
```
:scriptnames
:!set
:map
```
Load without vimrc and plugins : `vim --clean`

# ColorSchemes
## Manual install
install details, e.g. everforest
1. Download this package and extract it.
2. Copy `/path/to/everforest/autoload/everforest.vim` to `~/.vim/autoload`.
3. Copy `/path/to/everforest/colors/everforest.vim` to `~/.vim/colors/` .
4. Copy `/path/to/everforest/doc/everforest.txt` to `~/.vim/doc/` and execute `:helptags ~/.vim/doc/` to generate help tags. 
- [Everforest]( https://www.vim.org/scripts/script.php?script_id=5803 )

## After colors
Modify all the colorschemes by adding just what you wanna change, for example make everyone of them transparent `common.vim`:
```
hi Normal guibg=NONE ctermbg=NONE
```
install details
1. Put AfterColors.vim in ~/.vim/plugin/ or $HOME\vimfiles\plugin\
2. Create your colorscheme customizations in after/colors/<colors_name>.vim
3. Create global customizations (for all colorschemes) in after/colors/common.vim
- [After Colors]( https://www.vim.org/scripts/script.php?script_id=1641 )

## Installing for python
### From
windows: https://github.com/vim/vim-win32-installer/releases  
debian: apt install vim-nox  
### Check version and architecture  
`$ vim --version` should have: +python3 and  
windows: python##.dll  
debian: -L/usr/lib/python3.7/config-3.7m-x86_64-linux-gnu  
`:py3 import platform;print(platform.architecture())`  
`:py3 import sys;print(sys.version)`  
### Using python virtual environment?
`:py3 import sys;print(sys.prefix)`  
Activate the environment and run vim from that shell  
`:py3 import platform;print(platform.architecture())`  
