WINDOWS SETUP STEPS
# Desktop Shortcuts target
    gvim: "C:\Program Files (x86)\Vim\vim82\gvim.exe" -S %HOMEDRIVE%%HOMEPATH%\vimfiles\sessions\theSession.vim  
    
    (conda) cmd.exe: %windir%\System32\cmd.exe "/K" C:\Users\fdo\miniconda3\Scripts\activate.bat C:\Users\fdo\miniconda3  
    (conda) powershell: %windir%\System32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy ByPass -NoExit -Command "C:\Users\fdo\miniconda3\shell\condabin\conda-hook.ps1 ; conda activate;"  
    (conda) neovim: C:\Users\fdo\miniconda3\Scripts\activate.bat & conda activate & C:\Users\fdo\Source\nvim-win64\Neovim\bin\nvim-qt.exe  
    
    (py3env) cmd.exe: %windir%\System32\cmd.exe "/K" C:\Users\fdo\Source\py3env\Scripts\activate.bat C:\Users\fdo\  
    (py3env) powershell:  %windir%\System32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy ByPass -NoExit -Command "C:\Users\fdo\Source\py3env\Scripts\activate.ps1"  
    (py3env) neovim: C:\Users\fdo\Source\py3env\Scripts\activate.bat & C:\Users\fdo\Source\nvim-win64\Neovim\bin\nvim-qt.exe  

# Git
https://git-scm.com/download/win  
https://desktop.github.com/  
https://atom.io/  

# conda
https://docs.conda.io/en/latest/miniconda.html

    ## conda jupyterlab
	download install https://docs.conda.io/en/latest/miniconda.html Miniconda3-latest-Windows-x86_64.exe
    open anaconda shell (normal shell + eval(conda ...activate env base))
	conda create -n jupyter python3 ipython3 jupyterlab matplotlib numpy ipympl nodejs
	conda install -c conda-forge jupyterlab-git
	mkdir -p Library/LaunchAgents
	cp macos/conda-jupyter-startup.sh Library/LaunchAgents/.

	jupyter labextension install @jupyter-widgets/jupyterlab-manager @jupyterlab/toc jupyter-matplotlib qgrid2
	jupyter lab build

# python + pip : not working

 1. Python install for user  
 https://www.python.org/ftp/python/3.8.3/python-3.8.3-amd64.exe  

 2. Virtual environment  
 ```
python3 -m venv \~\Source\py3env  
cmd.exe \~$ Source\py3env\Scripts\activate.bat  
ps \~$ Source\py3env\Scripts\activate.ps1  
```  
 3. Cython  
 https://devblogs.microsoft.com/python/unable-to-find-vcvarsall-bat/  
 https://www.lfd.uci.edu/~gohlke/pythonlibs/  
     1. pip install SomeNumpy2.2.2.whl https://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy  
     2. vcred...exe https://www.microsoft.com/en-us/download/details.aspx?id=15336

 1. Git x2
https://git-scm.com/download/win  
https://desktop.github.com/

 1. [Node.js](https://nodejs.org/en/download/)

 1. Jupyterlab  

    > pip3 install jupyterlab jupyterlab-git jupyter-console matplotlib numpy pandas ipywidgets qgrid ipympl Cython PySide2  
    > jupyter labextension install jupyter-matplotlib qgrid2 @jupyter-widgets/jupyterlab-manager @jupyterlab/toc @jupyterlab/git  
    > jupyter lab build

 1. Vim bloated
https://www.vim.org/download.php#pc  
trying the stable https://ftp.nluug.nl/pub/vim/pc/gvim82.exe

 5. SpaceVim
    1. Install bloated Vim
    https://www.vim.org/download.php#pc  
    trying the stable https://ftp.nluug.nl/pub/vim/pc/gvim82.exe  

    2. Get command script
    https://spacevim.org/quick-start-guide/#windows
    > cmd.exe https://spacevim.org/install.cmd  
    > pip install --user flake8 isort yapf pynvim  
    > vim C:\Users\fdo\.SpaceVim.d\init.toml

    2. NeoVim
https://github.com/neovim/neovim/releases  
    > \~\AppData\Local\nvim\init.vim ~ vimrc
    > \~\.SpaceVim.d\init.toml <-  
    >
    > [[layers]]  
    > name="lang#python"  
    > format_on_save = 1  
    >
    > [[layers]]  
    > name = "fzf"  

    3. Not working with venv! [Jupyter-Console](https://github.com/jupyter-vim/jupyter-vim/)    

    > let g:python3_host_prog = 'C:\Users\fdo\Source\py3env\Scripts\python' -> init.vim

    4. Works! [Nvim-ipy](https://github.com/bfredl/nvim-ipy)

    > [[custom_plugins]]  
    > name = "bfredl/nvim-ipy" -> init.toml





# Dev python app, not working, not correct instructions

1. Download
https://www.python.org/ftp/python/3.8.3/python-3.8.3-embed-amd64.zip  

2. add the unzipped to PATH:  
c:\Users\fdo\Source\dev\windowsInstaller\python-3.8.3-embed-amd64

3. Download
https://pip.pypa.io/en/stable/installing/

   > WARNING: The scripts pip.exe, pip3.8.exe and pip3.exe are installed in 'C:\Users\fdo\Source\dev\windowsInstaller\python-3.8.3-embed-amd64\Scripts' which is not on PATH.  
  > Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.  
  > WARNING: The scripts easy_install-3.8.exe and easy_install.exe are installed in 'C:\Users\fdo\Source\dev\windowsInstaller\python-3.8.3-embed-amd64\Scripts' which is not on PATH.  
  > Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.  
  > WARNING: The script wheel.exe is installed in 'C:\Users\fdo\Source\dev\windowsInstaller\python-3.8.3-embed-amd64\Scripts' which is not on PATH.  
  > Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.  
  >
  > Successfully installed pip-20.1.1 setuptools-49.1.2 wheel-0.34.2  

4. python get-pip.py

5. add the Scripts to PATH too
c:\Users\fdo\Source\dev\windowsInstaller\python-3.8.3-embed-amd64\Scripts

# Dev

https://www.vim.org/download.php#pc

https://spacevim.org/quick-start-guide/#windows
