# Add theme to xfce4-terminal
```
mkdir -p ~/.local/share/xfce4/terminal/colorscheme
cp everforest-dark ~/.local/share/xfce4/terminal/colorscheme
cp everforest-light ~/.local/share/xfce4/terminal/colorscheme
```
# dark
```
[Scheme]
Name=Everforest-Dark
ColorForeground=#D3C6AA
ColorBackground=#333C43
TabActivityColor=#3F5865
ColorCursor=#D3C6AA
ColorSelection=#5C3F4F
ColorPalette=#4B565C;#E67E80;#A7C080;#DBBC7F;#7FBBB3;#D699B6;#83C092;#D3C6AA;#5C6A72;#F85552;#8DA101;#DFA000;#3A94C5;#DF69BA;#35A77C;#DFDDC8
```
# light
```
[Scheme]
Name=Everforest-Light
ColorForeground=#5C6A72
ColorBackground=#FDF6E3
TabActivityColor=#d8d8a9a97f7f
ColorCursor=#5C6A72
ColorSelection=#16163b3b5959
ColorPalette=#5C6A72;#F85552;#8DA101;#DFA000;#3A94C5;#DF69BA;#35A77C;#DFDDC8;#4B565C;#E67E80;#A7C080;#DBBC7F;#7FBBB3;#D699B6;#83C092;#D3C6AA
```
