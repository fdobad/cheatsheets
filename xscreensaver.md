
# status
query if dpms or screensaver are enabled
`xset -q`

If I do the following from terminal, then the screen turns off after 10 seconds of inactivity, as I expect.
`xset +dpms dpms 0 0 10`

`xset dpms force off`

# references
https://wiki.archlinux.org/title/Display_Power_Management_Signaling

